Tasks:

Asterisk means high priority

- [x] Add Restarts to EM algorithm \*
- [ ] Article -- Abstract \*
- [ ] Article -- Introduction \*
- [ ] Article -- Background \*
- [ ] Article -- Algorithms \*
- [ ] Article -- Results
- [ ] Article -- Conclusions
- [ ] Article -- Revision
- [ ] Check for quantization of image datasets (Olivetti/Pendigits)
- [ ] Compare MAP for image datasets
- [ ] Create charts comparing the lower/upper bound progress by time on the algorithms \*
- [ ] Limit time on running algorithms (2 hours) \*
- [ ] Compare the value found for MAP with the value found for Max-Product \*
- [ ] Describe the datasets for the Article
- [x] Remove cache on SPNs
- [ ] Check Gurobi/Coin|Or software (Academic versions)
- [ ] Check hdf5 for saving large SPNs